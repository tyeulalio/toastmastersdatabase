-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 14, 2017 at 06:05 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `glennga_DBA_TOAST`
--

-- --------------------------------------------------------

--
-- Table structure for table `AGENDA`
--

CREATE TABLE `AGENDA` (
  `AOrder` int(3) NOT NULL,
  `ITEM` varchar(50) NOT NULL,
  `ITEM_NUMBER` int(4) NOT NULL,
  `AType` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AGENDA`
--

INSERT INTO `AGENDA` (`AOrder`, `ITEM`, `ITEM_NUMBER`, `AType`) VALUES
(1, 'Location of Meeting', 16, 'ATTRIBUTE'),
(2, 'Presiding Officer', 1, 'DUTY'),
(3, 'Toastmaster of the Day', 2, 'DUTY'),
(4, 'Meeting Theme', 17, 'ATTRIBUTE'),
(5, 'Inspiration', 3, 'DUTY'),
(6, 'Jokemaster', 4, 'DUTY'),
(7, 'Grammarian', 5, 'DUTY'),
(8, 'Timer', 12, 'DUTY'),
(9, 'Ah-Counter', 11, 'DUTY'),
(10, 'Speaker1', 7, 'DUTY'),
(11, 'Title of Speech 1', 18, 'ATTRIBUTE'),
(12, 'Description of Speech 1', 19, 'ATTRIBUTE'),
(13, 'Speaker2', 8, 'DUTY'),
(14, 'Title of Speech 2', 20, 'ATTRIBUTE'),
(15, 'Description of Speech 2', 21, 'ATTRIBUTE'),
(16, 'Speaker3', 9, 'DUTY'),
(17, 'Title of Speech 3', 22, 'ATTRIBUTE'),
(18, 'Description of Speech 3', 23, 'ATTRIBUTE'),
(19, 'Evaluator1', 13, 'DUTY'),
(20, 'Evaluator2', 14, 'DUTY'),
(21, 'Evaluator3', 15, 'DUTY'),
(22, 'Table Topics Master', 6, 'DUTY'),
(23, 'General Evaluator', 10, 'DUTY');

--
-- Triggers `AGENDA`
--
DELIMITER $$
CREATE TRIGGER `NEW_AGENDA_CONFLICT` BEFORE INSERT ON `AGENDA` FOR EACH ROW BEGIN
SET @MD_TABLE = (SELECT EXISTS (SELECT 1 FROM MEETING_DUTIES WHERE ITEM_NUMBER = NEW.ITEM_NUMBER AND DUTY = NEW.ITEM));
SET @MA_TABLE = (SELECT EXISTS (SELECT 1 FROM MEETING_ATTRIBUTES WHERE ITEM_NUMBER = NEW.ITEM_NUMBER AND MAttribute = NEW.ITEM));

IF @MD_TABLE OR @MA_TABLE
THEN 
	IF ((@MD_TABLE AND NEW.AType = 'DUTY') OR (@MA_TABLE AND NEW.AType = 'ATTRIBUTE')) != 1
    THEN 
    	SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = 'Incorrect type.';
    END IF;
ELSE
	SIGNAL SQLSTATE '45000'
    	SET MESSAGE_TEXT = 'Duty, attribute, or item number does not exist.';
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `UPDATE_AGENDA_CONFLICT` BEFORE UPDATE ON `AGENDA` FOR EACH ROW BEGIN
SET @MD_TABLE = (SELECT EXISTS (SELECT 1 FROM MEETING_DUTIES WHERE ITEM_NUMBER = NEW.ITEM_NUMBER AND DUTY = NEW.ITEM));
SET @MA_TABLE = (SELECT EXISTS (SELECT 1 FROM MEETING_ATTRIBUTES WHERE ITEM_NUMBER = NEW.ITEM_NUMBER AND MAttribute = NEW.ITEM));

IF @MD_TABLE OR @MA_TABLE
THEN 
	IF ((@MD_TABLE AND NEW.AType = 'DUTY') OR (@MA_TABLE AND NEW.AType = 'ATTRIBUTE')) != 1
    THEN 
    	SIGNAL SQLSTATE '45000'
        	SET MESSAGE_TEXT = 'Incorrect type.';
    END IF;
ELSE
	SIGNAL SQLSTATE '45000'
    	SET MESSAGE_TEXT = 'Duty, attribute, or item number does not exist.';
END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AGENDA`
--
ALTER TABLE `AGENDA`
  ADD PRIMARY KEY (`AOrder`,`ITEM`,`ITEM_NUMBER`) USING BTREE,
  ADD UNIQUE KEY `AOrder` (`AOrder`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
