-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2017 at 07:39 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `glennga_DBA_TOAST`
--

-- --------------------------------------------------------

--
-- Table structure for table `MEETING_DUTIES`
--

CREATE TABLE `MEETING_DUTIES` (
  `DUTY` varchar(50) NOT NULL,
  `ITEM_NUMBER` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MEETING_DUTIES`
--

INSERT INTO `MEETING_DUTIES` (`DUTY`, `ITEM_NUMBER`) VALUES
('Presiding Officer', 1),
('Toastmaster of the Day', 2),
('Inspiration', 3),
('Jokemaster', 4),
('Grammarian', 5),
('Table Topics Master', 6),
('Speaker1', 7),
('Speaker2', 8),
('Speaker3', 9),
('General Evaluator', 10),
('Ah-Counter', 11),
('Timer', 12),
('Evaluator1', 13),
('Evaluator2', 14),
('Evaluator3', 15);

--
-- Triggers `MEETING_DUTIES`
--
DELIMITER $$
CREATE TRIGGER `ITEM_NUMBER_CONFLICT_DUT` BEFORE INSERT ON `MEETING_DUTIES` FOR EACH ROW BEGIN
IF (SELECT EXISTS (SELECT 1 FROM MEETING_ATTRIBUTES WHERE ITEM_NUMBER = NEW.ITEM_NUMBER))
THEN 
	SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'ITEM_NUMBER not unique.';
END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `MEETING_DUTIES`
--
ALTER TABLE `MEETING_DUTIES`
  ADD PRIMARY KEY (`DUTY`),
  ADD UNIQUE KEY `ITEM_NUMBER` (`ITEM_NUMBER`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
