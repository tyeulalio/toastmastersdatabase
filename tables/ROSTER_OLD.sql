-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 09, 2017 at 08:53 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eulalio_toastmasters`
--

-- --------------------------------------------------------

--
-- Table structure for table `ROSTER`
--

CREATE TABLE `ROSTER` (
  `TID` int(11) NOT NULL,
  `RKey` varchar(3) CHARACTER SET utf8 DEFAULT NULL,
  `FirstName` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `LastName` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `Active` int(11) NOT NULL DEFAULT '0',
  `Title` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `Speeches` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `HomePhone` varchar(12) CHARACTER SET utf8 DEFAULT NULL,
  `WorkPhone` varchar(22) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(33) CHARACTER SET utf8 DEFAULT NULL,
  `Address` varchar(29) CHARACTER SET utf8 DEFAULT NULL,
  `Address2` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `City` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `State` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  `Zipcode` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `Notes` varchar(52) CHARACTER SET utf8 DEFAULT NULL,
  `Password` varchar(26) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ROSTER`
--

INSERT INTO `ROSTER` (`TID`, `RKey`, `FirstName`, `LastName`, `Active`, `Title`, `Speeches`, `HomePhone`, `WorkPhone`, `Email`, `Address`, `Address2`, `City`, `State`, `Zipcode`, `Notes`, `Password`) VALUES
(1, 'AA1', 'Amber', 'Abinsay', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'AA2', 'Anita', 'Akel-Soileau', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'AB1', 'Amy', 'Blackwell', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'AC1', 'Alex', 'Cates', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'AC2', 'Alex', 'Cates', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'AE1', 'Alyson', 'Emde', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'AG1', 'Archie', 'Grey', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'AJ1', 'Audrey', 'Jewell', 1, 'TM', '0', '808-295-3482', '', 'amjewell@hawaii.rr.com', '', '', '', 'HI', '96', '', '00N4JWWWC6fDg'),
(9, 'AM1', 'Alli', 'Moloney', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'AP1', 'Anthony', 'Poponi', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'AR1', 'Alberto', 'Ricordi', 1, 'TM', '0', '808-783-2499', '', 'albertoricordi@gmail.com', '', '', '', '', '', '', '00k8Bt8Jm8Uso'),
(12, 'AY1', 'Adelina', 'Yim', 1, 'TM', '0', '371-3510', '371-3510', 'adelinayim@gmail.com', '', '', '', '', '', '', '00tiiv3BFcgFs'),
(13, 'B 1', 'Breck', 'Dangler', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'BB1', 'Blaise K.', 'Bissen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'BH1', 'Bart', 'Howk', 1, 'TM', '0', '8086363133', '', 'bhowk@legalhawaii.com', '', '', '', 'HI', '96', '', '00PEkUPG98DZM'),
(16, 'BI1', 'Bev', 'Iraha', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'BM1', 'Bhawna', 'Mundotia', 1, 'TM', '0', '4055418936', '', 'bhawna.mundotia@gmail.com', '', '', '', 'HI', '96', '', '00JPxS/wJ.xPw'),
(18, 'BT1', 'Brian', 'Takahashi', 1, 'ATM- B', '10', '735-3991', '523-9636', 'btakahashi@ahldesign.com', '733 Bishop St., Ste. 3100', '', 'Honolulu', 'HI', '96813', '96813', '00j.HvVu2wm6.'),
(19, 'CA1', 'Christopher', 'Akin', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'CB1', 'Clara', 'Barnes', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'CC1', 'Connie', 'Chang', 1, 'TM', '2', '589-1458', '544-8336', 'mail.connie@yahoo.com', '999 Bishop Street, 23rd Floor', '', 'Honolulu', 'HI', '96814', '', '00fRYziAKQtko'),
(22, 'CC2', 'Charles', 'Churchill', 1, 'CTM', '10', '479-6603', '441-4368', 'chip@honblue.com', '', '', '', '', '', '', '00RGNtz2ieLkc'),
(23, 'CH1', 'Christopher', 'Haig', 1, 'TM', '0', '808-497-4100', '', 'Cjd.haig@gmail.com', '', '', '', 'HI', '96', '', '00HXJLTgfts.o00hfw5DfSJZ3o'),
(24, 'CH2', 'Cara', 'Hiyakumoto', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'CH3', 'Cara', 'Odo', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'CL1', 'Chris', 'Lum', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'CM1', 'Christine', 'Medeiros', 1, 'TM', '4', '808-373-0636', '', 'cheng.chris@gmail.com', '1238 Manulani Street', '', 'Kailua', 'HI', '96734', 'Christine Medeiros cheng.chris@gmail.com\\nChristine1', '00j2jrVt3ions0070qKmmfBM3Y'),
(28, 'CN1', 'Cheryl', 'Nishita', 1, 'TM', '0', '', '(808) 561-1904', 'cheryl.nishita@boh.com', '750 Puu Hina Place', '', 'Pearl City', 'HI', '96782', '', '00rjgHtOk3nS.'),
(29, 'CO1', 'Cullen', 'Oesterly', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'CP1', 'Clara', 'Park', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'CP2', 'Christopher', 'Procopio', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'CS1', 'Christopher', 'Steadley', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'CW1', 'Charmaine', 'Williams', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'CY1', 'Cheryl', 'Yeh', 1, 'ATMB', '10', '395-5257', '544-3557', 'cheryl.yeh@centralpacificbank.com', '', '', '', '', '', 'has previous Toastmaster experience', '00bUVqXEsow7.'),
(35, 'DB1', 'Don', 'Baluran', 1, 'ACB, ALB', '20', '808-638-9819', '', 'dbaluran@asbhawaii.com', '', '', '', '', '', '', '00BljbJzG/zNo'),
(36, 'DC1', 'Demetria', 'Caston', 1, 'TM', '0', '', '214-289-2596', 'dcaston@hawaii.wish.org', '', '', '', 'HI', '96', '', '004vFaifViSUg00uPJwdKC7nbU'),
(37, 'DD1', 'Daniel', 'Davidson', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'DF1', 'Doug', 'Forrest', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'DH1', 'Duke', 'Hashimogo', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'DK1', 'Debra', 'Kohn-Lee', 1, 'TM', '1', '', '808-778-6719', 'playinwithdeb@yahoo.com', '1212 Nuuanu Ave. #2903', '', 'Honolulu', 'HI', '96817', '', '00hMdxH1fTrPo'),
(41, 'DL1', 'Daena', 'Lee', 1, 'TM', '6', '808-638-1820', '808-833-2225x1032', 'daena.lee@gmail.com', '55 S Kukui St', 'Apt 3202', 'Honolulu', 'Hawaii (HI)', '96813', '', '00q7.sB46G86c'),
(42, 'DL2', 'Dang', 'Le', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'DM1', 'Daniel', 'Marcom', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'DO1', 'Dave', 'Oberheu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'DP1', 'Dave', 'Palomares', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'DS1', 'Danielle', 'Scherman', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'dw1', 'Derek', 'Wickes', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'DW2', 'Daniel', 'Wilson', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'EC1', 'Eric', 'Collins', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'EF1', 'Ernest', 'Fulton', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'EI1', 'Elden', 'Ito', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'EM1', 'Erin', 'Marquez', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'EM2', 'Elya', 'McCleave', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'ET1', 'Eric', 'Taramasco', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'EZ1', 'Elle', 'Zhang', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'FK1', 'Fay', 'Kauanoe', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'GC1', 'Greg', 'Ching', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'GD1', 'Greg', 'Dalin', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'GK1', 'Gregory', 'Kessler', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'GL1', 'Gabriela', 'Layi', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'GL2', 'Gabriela', 'Layi', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'GN1', 'Gabriela', 'Noriega', 1, 'TM', '2', '8082780265', '', 'gabrielanoriega@gmail.com', '', '', '', 'HI', '96', '', '00jEgVlChrMxM00uPJwdKC7nbU'),
(63, 'GO1', 'Gaylord', 'Oshiro', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'GO2', 'Gerald', 'Ossy', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'GP1', 'Grace', 'Paul', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'GR1', 'Gregg', 'Robertson', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'GS1', 'Glenn', 'Simcox', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'GW1', 'Grace', 'Wong', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'GY1', 'Greg', 'Yuen', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'GY2', 'Grace', 'Yu-Cua', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'GY3', 'Gavin', 'Yukitomo', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'HA1', 'Hillary', 'Agra', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'HH1', 'Haku', 'Hoopai', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'HK1', 'Harvey', 'Ku', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'HN1', 'Heidi', 'Naea', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'HP1', 'Hieu', 'Pham Stuart', 1, 'ACB, CL', '', '8083437713', '', 'hieu0115@gmail.com', '828 A Kanoa St', 'A', 'Honolulu', 'Hi', '96817', 'Joined 9/1/2010', '00w.jM2jZFSCY'),
(77, 'HP2', 'Huong', 'Pham', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'HR1', 'Harvey', 'Rackmil', 1, 'CTM', '10', '732-9707', '441-4471', 'harvey@honblue.com', '', '', '', '', '', '', '009EQb2aED1mI'),
(79, 'HS1', 'Helen', 'Smalley-Bower', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 'IC1', 'Irene', 'Chun', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 'IW1', 'Ina', 'Wong', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'JC1', 'Jacob', 'Chase', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 'JC2', 'Johnny', 'Cantillo', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 'JD1', 'Jeff', 'Davis', 1, 'TM', '9', '808-859-8970', '808-859-8970', 'Jeff.davisusmc@gmail.com', 'PO Box 372201', '', 'Honolulu', 'Hawaii', '96837', '', '00CQuYabu4pEA'),
(85, 'JD2', 'James', 'Donnelly', 1, 'TM', '0', '8083499436', '', 'james.donnellyjr@gmail.com', '', '', '', 'HI', '96', '', '00yWOXQWwifpk00uPJwdKC7nbU'),
(86, 'JE1', 'John', 'Ellis', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'JE2', 'Joseph', 'Edward', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'JE3', 'Jeff', 'Esmond', 1, 'TM', '9', '', '392-9324', 'Jeff.esmond@gmail.com', '', '', '', '', '', '', 'kepi237'),
(89, 'JF1', 'Joel', 'Francisco', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 'JG1', 'Jason', 'Gaspero', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 'JH1', 'John', 'Holman', 1, 'TM', '0', '630-3807', '522-8041', 'john.holman@outlook.com', '', '', '', '', '', '', '00aT4LgVjJbZY'),
(92, 'JH2', 'Joel', 'Hatch', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 'JJ1', 'Duff', 'Janus', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 'JM1', 'Jordan', 'Mossman', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 'JO1', 'John', 'Ogoshi', 1, 'TM', '1', '808-799-7200', '808-545-6442', 'johnogoshi@hotmail.com', '', '', '', 'HI', '96', '', '001ku/TzsmM5g'),
(96, 'JO2', 'Jin', 'Oh', 1, 'TM', '0', '808-392-7028', '', 'jinoh0304@gmail.com', '', '', '', 'HI', '96', '', '00asFwSd/ttYw'),
(97, 'JP1', 'Jane', 'Preece', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 'JQ1', 'John', 'Quitoriano', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 'JR1', 'Jason', 'Ruston', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 'JS1', 'Julius', 'Serrano', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 'JS2', 'James', 'Smith', 1, 'CC', '10', '808-358-1344', '808-732-7227', 'james@indurate.com', '569 Kapahulu Ave', '', 'Honolulu', 'HI', '96815', '', '004K4vWbcv2Dg'),
(102, 'JS3', 'James', 'Sweet', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 'JT1', 'Jenny', 'Teo', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 'JU1', 'Judy', 'Ung', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 'kA1', 'Kimberly', 'Aquino', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 'KC1', 'Kevin', 'Cheong', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 'KC2', 'Keith', 'Coronel', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 'KD1', 'Kathy', 'Doering', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 'KH1', 'Kenneth', 'Hu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 'KJ1', 'Karen', 'Jones', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 'KM1', 'Kenneth', 'Mansfield', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 'KO1', 'Kylie', 'Ota', 1, 'TM', '0', '8082282721', '', 'kylie@alohaunveiled.com', '', '', '', 'HI', '96', '', '00qAKgGSRIQ72'),
(113, 'KR1', 'Keiko', 'Rodrigues', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 'KS1', 'Katie', 'Stula', 1, 'TM', '4', '415-425-4495', '888-1177', 'katie.slawinski@hawaiiantel.com', '', '', 'Honolulu', 'HI', '96825', 'Katie Stula, alternate email:\\nkatie.stula@gmail.com', '00G5tQx0XtnqQ'),
(115, 'KS2', 'Kate', 'Sweeney', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 'KT1', 'Katie', 'Tangtipongkul', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 'KW1', 'Koa', 'Webster', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'KY1', 'Katie', 'Young', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 'KZ1', 'Kelu', 'Zhang', 1, 'TM', '0', '808-237-0707', '', 'kelu_zhang@hotmail.com', '', '', '', 'HI', '96', '', '00D87qjz63zVE'),
(120, 'LA1', 'Lani', 'Abrigana', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 'LG1', 'Leah', 'Gigante', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 'LG2', 'Liang', 'Ge', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 'LH1', 'Lee', 'Hopkinson', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 'LK1', 'Leslie', 'Kurisaki', 1, 'TM', '0', '', '(808) 284-2111', 'lkurisaki@hhf.com', '', '', '', '', '', 'Leslie Kurisaki\\nlkurisaki@hhf.com', '00L7k5.ZbIEUA'),
(125, 'LK2', 'Lauren', 'Kanae-Firestone', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 'LP1', 'Leslie', 'Patacsil', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 'LS1', 'Laura', 'Stone-Jeraj', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 'LT1', 'Laron', 'Tamaye', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 'MA1', 'Maurice', 'Anstead', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 'MB1', 'Mark', 'Blackburn', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 'MC1', 'Marilyn', 'Cariaga', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 'MC2', 'Maria Clara', 'Canoy', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 'ME1', 'Minoo', 'Elison', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 'MF1', 'Matthias', 'Feldmann', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 'MG1', 'Matthew', 'Grieder', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 'MH1', 'Mark', 'Hironaga', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 'MK1', 'Matthew', 'Kotelnicki', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 'MM1', 'Matthew', 'Meelee', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 'MR1', 'Max', 'Redden', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, 'MS1', 'Mary', 'St. John', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, 'MT1', 'Matthew', 'Takamatsu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, 'MT2', 'Matthew', 'Takamatsu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 'MU1', 'Monique', 'Underwood', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 'MW1', 'Melissa', 'Wageman', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 'NH1', 'Norman', 'Hirohata-Goto', 1, 'CTM', '10', '373-5777', '533-2261#Fax: 536-1751', 'nihg@hawaii.rr.com', '', '', '', '', '', '', '00unesYvMKvPc00Ie4832jm0IE'),
(146, 'NH2', 'Nick', 'Huddleston', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 'NJ1', 'Natalie', 'Jordan', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, 'NP1', 'Nicia', 'Platt', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 'NR1', 'Nicole', 'Ruston', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, 'ny1', 'nick', 'youngleson', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 'OC1', 'Ohira', 'Chad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 'OM1', 'Otto', 'Mazenauer', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, 'PA1', 'Paul', 'Anderson', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, 'PB1', 'Pascal', 'Bolomet', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(155, 'PC1', 'Pete', 'Cooper', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 'PF1', 'Peter', 'Forman', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 'PH1', 'Phillip', 'Hasha', 1, 'TM', '0', '', '(808) 253-1959', 'phillip@redmontgroup.com', '', '', '', 'HI', '96', '', '0041MCXSMj6/Y'),
(158, 'PL1', 'Patricia', 'LaPorte', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 'PR1', 'Pam', 'Roth', 1, 'TM', '0', '808-690-2961', '', 'pjtee7@gmail.com', '', '', '', 'HI', '', '', '00xqCNMF55bD6'),
(160, 'RC1', 'Robert', 'Crozier', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, 'RD1', 'Rocky', 'Davis', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, 'RE1', 'Robert', 'Erb', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, 'RF1', 'Rechung', 'Fujihira', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, 'RH1', 'Rich', 'Halverson', 1, 'CTM', '10', '808-371-7424', '808-371-7424', 'richhalverson@gmail.com', '1180 Mokuhano St', '', 'Honolulu', 'HI', '96825', '', '00KXmTOM3wUOU'),
(165, 'RS1', 'Richard', 'Sullivan', 1, 'ATM-B', '10', '342-1130', '523-9636', 'richsully1942@gmail.com', '', '', '', '', '', '', '00t.1QvztA9Es'),
(166, 'RS2', 'Bob', 'Stott', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, 'RS3', 'Ryan', 'Strada', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, 'RT1', 'Renee', 'Thomas', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, 'RW1', 'Rebecca', 'Weatherford', 1, 'TM', '10', '554-9976', '523-9636', 'rweatherford@ahldesign.com', '', '', '', '', '', '', '00Sn76k8QJR6w'),
(170, 'RW2', 'Robert', 'Wells', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, 'RY1', 'Robyn', 'Yee', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, 'SA1', 'Scott', 'Aldinger', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, 'SB1', 'Sequoya', 'Borgman', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, 'SC1', 'Stanley', 'Calma', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, 'SD1', 'Sean', 'Do', 1, 'TM', '0', '', '', 'sdo@caahawaii.com', '', '', '', '', '', '', '000LaN7ot/dhM'),
(176, 'SD2', 'Shiraz', 'Dole', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, 'SJ1', 'Stanley', 'Jacobs', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, 'SJ2', 'Stan', 'Jacobs', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, 'SK1', 'Stacy', 'Kracher', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, 'SL1', 'Shao Yu', 'Lee', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, 'SL2', 'Susan', 'Lam', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(182, 'SM1', 'Sibel', 'Mestanova', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, 'SP1', 'Savan', 'Patel', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, 'SR1', 'Shawn', 'Richards', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, 'SV1', 'Snider', 'Vick', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, 'SY1', 'Shere\'e', 'Young', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, 'TB1', 'Tina', 'Bae', 1, 'TM', '9', '917-349-1681', '792-7335', 'tinabae@gmail.com', '', '', '', 'HI', '96', '', '004w/s7wnbHhM'),
(188, 'TE1', 'Tamara', 'Edwards', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, 'TI2', 'Tom', 'Ihsle', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, 'TK1', 'Tony', 'Kayatani', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, 'TM1', 'Tabitha', 'Mitchell', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, 'TO1', 'Tyler', 'Olds', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, 'TS1', 'Tiffany', 'Scheffer', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, 'TT1', 'Trent', 'Thoms', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, 'TT2', 'Trent', 'Thoms', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, 'TW1', 'Terry', 'Wade', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, 'TW2', 'Terry', 'Wade', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, 'TY1', 'Tom', 'Yamachika', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, 'UR1', 'Ute', 'Regan', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, 'VK1', 'Valerie', 'Koenig', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(201, 'WB1', 'Wendy', 'Baldwin', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(202, 'WD1', 'Warren', 'Delarosa', 1, 'TM', '0', '', '', 'delarosawarren@hotmail.com', '', '', '', 'HI', '', '', '00wbgTf8gL5S.'),
(203, 'WH1', 'Wen', 'He', 1, 'ACS, CL', '10', '393-8010', '202-5254', 'hewen@hotmail.com', '', '', '', '', '', 'Entered 4/8/2011', '00Wp3/UyYs3ZM00J.gmGBDid6c'),
(204, 'WM1', 'Wendie', 'McAllaster', 1, 'TM', '0', '8082263500', '', 'wmcallaster@hhf.com', '', '', '', 'HI', '96', '', '00ip9zrlGCU/k'),
(205, 'WS1', 'William', 'Sink', 1, 'TM', '0', '808-531-7162', '808-531-7162', 'jennifer@wfsinklaw.com', '735 Bishop Street, Ste. 400', '', 'Honolulu', 'HI', '96821', '', '00AYIDxUl8eU.'),
(206, 'XR1', 'Xenia', 'Regalia', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, 'XS1', 'Xiaoyan (Maria)', 'Su', 1, 'TM', '3', '5107251816', '5107251816', 'maria.su@spirehi.com', '3030 Lowrey Ave', '104', 'Honolulu', 'Hawaii', '96822', '', '00kiAoTAa8Vqc'),
(208, 'YZ1', 'Yongqin', 'Zhu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, 'ZL1', 'Zachary', 'Lee', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ROSTER`
--
ALTER TABLE `ROSTER`
  ADD PRIMARY KEY (`TID`),
  ADD UNIQUE KEY `RKey` (`RKey`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ROSTER`
--
ALTER TABLE `ROSTER`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
