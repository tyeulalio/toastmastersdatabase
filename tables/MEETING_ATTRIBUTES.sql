-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2017 at 07:38 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `glennga_DBA_TOAST`
--

-- --------------------------------------------------------

--
-- Table structure for table `MEETING_ATTRIBUTES`
--

CREATE TABLE `MEETING_ATTRIBUTES` (
  `MAttribute` varchar(50) NOT NULL,
  `ITEM_NUMBER` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MEETING_ATTRIBUTES`
--

INSERT INTO `MEETING_ATTRIBUTES` (`MAttribute`, `ITEM_NUMBER`) VALUES
('Location of Meeting', 16),
('Meeting Theme', 17),
('Title of Speech 1', 18),
('Description of Speech 1', 19),
('Title of Speech 2', 20),
('Description of Speech 2', 21),
('Title of Speech 3', 22),
('Description of Speech 3', 23);

--
-- Triggers `MEETING_ATTRIBUTES`
--
DELIMITER $$
CREATE TRIGGER `ITEM_NUMBER_CONFLICT_ATT` BEFORE INSERT ON `MEETING_ATTRIBUTES` FOR EACH ROW BEGIN
IF (SELECT EXISTS (SELECT 1 FROM MEETING_DUTIES WHERE ITEM_NUMBER = NEW.ITEM_NUMBER))
THEN 
	SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'ITEM_NUMBER not unique.';
END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `MEETING_ATTRIBUTES`
--
ALTER TABLE `MEETING_ATTRIBUTES`
  ADD PRIMARY KEY (`MAttribute`),
  ADD UNIQUE KEY `ITEM_NUMBER` (`ITEM_NUMBER`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
