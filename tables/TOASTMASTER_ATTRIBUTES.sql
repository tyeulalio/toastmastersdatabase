-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2017 at 09:21 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eulalio_toastmasters`
--

-- --------------------------------------------------------

--
-- Table structure for table `TOASTMASTER_ATTRIBUTES`
--

CREATE TABLE `TOASTMASTER_ATTRIBUTES` (
  `T_ATTRIBUTE` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TOASTMASTER_ATTRIBUTES`
--

INSERT INTO `TOASTMASTER_ATTRIBUTES` (`T_ATTRIBUTE`) VALUES
('Active'),
('City'),
('Email'),
('FirstName'),
('HomePhone'),
('LastName'),
('Password'),
('State'),
('StreetAddress'),
('Title'),
('WorkPhone'),
('Zipcode');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `TOASTMASTER_ATTRIBUTES`
--
ALTER TABLE `TOASTMASTER_ATTRIBUTES`
  ADD PRIMARY KEY (`T_ATTRIBUTE`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
