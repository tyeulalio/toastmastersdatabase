<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/logout.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without begin logged in.
session_unset();
session_destroy();
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test after begin logged in.
$_REQUEST['TID'] = '8';
$_REQUEST['Password'] = '00N4JWWWC6fDg';
echo 'Logging In...<br>';
include('../api/login.php');
echo '<br><br>';
echo 'Test 2: Happy Path, Logged In' . '<br>';
perform_test();
?>