<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/get_agenda.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without any parameters.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test with MDate specified.
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 2: Happy Path, Correct MDate Specified' . '<br>';
perform_test();

// Test with incorrect date format.
$_REQUEST['MDate'] = '20010110';
echo 'Test 3: Incorrect Date Format' . '<br>';
perform_test();

// Test with a date for a meeting that does not exist.
$_REQUEST['MDate'] = '2001-01-09';
echo 'Test 4: Meeting Does Not Exist' . '<br>';
perform_test();
?>