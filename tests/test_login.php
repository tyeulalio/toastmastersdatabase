<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/login.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without any parameters.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test with only a valid TID.
$_REQUEST['TID'] = '8';
echo 'Test 2: Only Valid TID' . '<br>';
perform_test();

// Test with only a password specified.
$_REQUEST['Password'] = 'somepassword';
echo 'Test 3: Only Password Specified' . '<br>';
perform_test();

// Test with a valid TID and password.
$_REQUEST['TID'] = '8';
$_REQUEST['Password'] = '00N4JWWWC6fDg';
echo 'Test 4: Happy Path, All Input Correct' . '<br>';
perform_test();

// Test with a user that does not have a password.
$_REQUEST['TID'] = '1';
$_REQUEST['Password'] = '';
echo 'Test 5: No User Password' . '<br>';
perform_test();

// Test with a user that has a slash in their password.
$_REQUEST['TID'] = '95';
$_REQUEST['Password'] = '001ku/TzsmM5g';
echo 'Test 6: Slash In Password' . '<br>';
perform_test();

// Test with a user that has a password, and specifies wrong password.
$_REQUEST['TID'] = '8';
$_REQUEST['Password'] = '00N4JWWWsssssC6fDg';
echo 'Test 7: Wrong Password' . '<br>';
perform_test();

// Test with a user that has a password, and specifies the blank password.
$_REQUEST['TID'] = '8';
$_REQUEST['Password'] = '';
echo 'Test 8: Blank Password' . '<br>';
perform_test();
?>