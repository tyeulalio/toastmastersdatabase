<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/get_meetings.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without any parameters.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test with only beginning date specified.
$_REQUEST['DateBegin'] = '2001-01-10';
echo 'Test 2: Only DateBegin Specified' . '<br>';
perform_test();

// Test with only end date specified.
$_REQUEST['DateEnd'] = '2001-01-10';
echo 'Test 3: Only DateEnd Specified' . '<br>';
perform_test();

// Test with the later date at the beginning, and vice versa.
$_REQUEST['DateBegin'] = '2001-01-31';
$_REQUEST['DateEnd'] = '2001-01-10';
echo 'Test 4: Dates Are Switched' . '<br>';
perform_test();

// Test with beginning date = end date.
$_REQUEST['DateBegin'] = '2001-01-10';
$_REQUEST['DateEnd'] = '2001-01-10';
echo 'Test 5: DateBegin = DateEnd' . '<br>';
perform_test();

// Test with incorrect date formats.
$_REQUEST['DateBegin'] = '20010110';
$_REQUEST['DateEnd'] = '20010131';
echo 'Test 6: Incorrect Date Format' . '<br>';
perform_test();

// Test with correct beginning and end date.
$_REQUEST['DateBegin'] = '2001-01-10';
$_REQUEST['DateEnd'] = '2001-01-31';
echo 'Test 7: Happy Path, Correct DateBegin and DateEnd' . '<br>';
perform_test();
?>