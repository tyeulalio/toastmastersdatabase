<?php
include('../api/_global.php');

/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test()
{
    include('../api/update_agenda.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

/**
 * Update our data.
 *
 * @param $c mysqli Connection object to our database.
 */
function update_entries($c)
{
    if ($c->query("UPDATE SCHEDULE SET MID = 1, ITEM_NUMBER = 17, TID = 0, TEXT = '2001: A Speech Odyssey'
                          WHERE MID = 1 AND ITEM_NUMBER = 17")) {
        echo 'Item 1 Reinserted...<br>';
    } else die ('Error: ' . $c->error);

    if ($c->query("UPDATE SCHEDULE SET MID = 1, ITEM_NUMBER = 7, TID = 69, TEXT = 'Greg Yuen'
                          WHERE MID = 1 AND ITEM_NUMBER = 7")) {
        echo 'Item 2 Reinserted...<br><br>';
    } else die ('Error: ' . $c->error);
}

/**
 * Remove the items we input.
 *
 * @param $c mysqli Connection object to our database.
 */
function remove_entries($c)
{
    if ($c->query("DELETE FROM SCHEDULE WHERE MID = 1 AND (ITEM_NUMBER = 20 OR ITEM_NUMBER = 3)")) {
        echo 'Items Deleted...<br><br>';
    } else die ('Error: ' . $c->error);
}

remove_entries($conn);
update_entries($conn);

// Test without any parameters.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test with only MDate specified.
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 2: Only MDate Specified' . '<br>';
perform_test();

// Test with MDate and empty NewItemList and NewEntryList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = '';
$_REQUEST['NewEntryList'] = '';
echo 'Test 3: Correct MDate, Empty Lists' . '<br>';
perform_test();

// Test with MDate and an incorrect item in NewItemList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiratioasdsadasaddn';
$_REQUEST['NewEntryList'] = 'Hello,123';
echo 'Test 4: Incorrect Item in RemoveItemList' . '<br>';
perform_test();
remove_entries($conn);

// Test with correct MDate, NewItemList, and NewEntryList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiration';
$_REQUEST['NewEntryList'] = 'Hello,123';
echo 'Test 5: Happy Path Insert, Correct MDate and Lists' . '<br>';
perform_test();

// Attempt to insert again.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiration';
$_REQUEST['NewEntryList'] = 'Hello,123';
echo 'Test 6: Insert Again' . '<br>';
perform_test();
remove_entries($conn);

// Test with non-matching list lengths.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiration';
$_REQUEST['NewEntryList'] = 'Hello';
echo 'Test 7: Lists Lengths Do Not Match' . '<br>';
perform_test();

// Test with incorrect date format.
$_REQUEST['MDate'] = '20010110';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiration';
$_REQUEST['NewEntryList'] = 'Hello,123';
echo 'Test 8: Incorrect Date Format' . '<br>';
perform_test();

// Test with a date for a meeting that does not exist.
$_REQUEST['MDate'] = '2001-01-09';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiration';
$_REQUEST['NewEntryList'] = 'Hello,123';
echo 'Test 9: Meeting Does Not Exist' . '<br>';
perform_test();

// Test happy path update existing entries.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Meeting Theme,Speaker1';
$_REQUEST['NewEntryList'] = 'SomeThing, 3';
echo 'Test 10: Happy Path Update, Correct MDate and Lists' . '<br>';
perform_test();

// Attempt to update again.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Meeting Theme,Speaker1';
$_REQUEST['NewEntryList'] = 'SomeThing, 3';
echo 'Test 11: Update Again' . '<br>';
perform_test();
update_entries($conn);

// Update with member that does not exist.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Meeting Theme,Speaker1';
$_REQUEST['NewEntryList'] = 'SomeThing, 1111';
echo 'Test 12: Updating With Member that Does Not Exist' . '<br>';
perform_test();

// Update with member that does not exist.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['NewItemList'] = 'Title of Speech 2,Inspiration';
$_REQUEST['NewEntryList'] = 'Hello,1111';
echo 'Test 13: Inserting With Member that Does Not Exist' . '<br>';
perform_test();

// Test with MDate and empty item in RemoveItemList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['RemoveItemList'] = 'Meeting Theme,Speaker1,';
echo 'Test 14: Extra Comma in RemoveItemList' . '<br>';
perform_test();
?>