<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/get_availability_user.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without any parameters, not logged in.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test without parameters, logged in.
$_REQUEST['TID'] = '66';
$_REQUEST['Password'] = '';
echo 'Logging In...<br>';
include('../api/login.php');
echo '<br><br>';
echo 'Test 2: No Parameters, Logged In' . '<br>';
perform_test();

// Test with only beginning date specified.
$_REQUEST['DateBegin'] = '2001-01-10';
echo 'Test 3: Only DateBegin Specified' . '<br>';
perform_test();

// Test with only end date specified.
$_REQUEST['DateEnd'] = '2001-01-10';
echo 'Test 4: Only DateEnd Specified' . '<br>';
perform_test();

// Test with the later date at the beginning, and vice versa.
$_REQUEST['DateBegin'] = '2001-01-31';
$_REQUEST['DateEnd'] = '2001-01-10';
echo 'Test 5: Dates Are Switched' . '<br>';
perform_test();

// Test with beginning date = end date.
$_REQUEST['DateBegin'] = '2001-01-10';
$_REQUEST['DateEnd'] = '2001-01-10';
echo 'Test 6: DateBegin = DateEnd' . '<br>';
perform_test();

// Test with incorrect date formats.
$_REQUEST['DateBegin'] = '20010110';
$_REQUEST['DateEnd'] = '20010131';
echo 'Test 7: Incorrect Date Format' . '<br>';
perform_test();

// Test with correct beginning and end date.
$_REQUEST['DateBegin'] = '2005-01-01';
$_REQUEST['DateEnd'] = '2005-03-04';
echo 'Test 8: Happy Path, Correct DateBegin and DateEnd' . '<br>';
perform_test();
?>