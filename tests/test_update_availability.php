<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/update_availability.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without any parameters, and not logged in.
session_unset();
session_destroy();
echo 'Test 1: No Parameters, Not Logged In' . '<br>';
perform_test();

// Test with correct parameters, and not begin logged in.
$_REQUEST['Function'] = 'Insert';
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 2: Correct Parameters, Not Logged In' . '<br>';
perform_test();

// Test without any parameters, and begin logged in.
$_REQUEST['TID'] = '8';
$_REQUEST['Password'] = '00N4JWWWC6fDg';
echo 'Logging In...<br>';
include('../toast/login.php');
echo '<br><br>';
echo 'Test 3: No Parameters, Logged In' . '<br>';
perform_test();

// Test with incorrect function specified, but correct MDate.
$_REQUEST['Function'] = 'HelloThere';
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 4: Incorrect Function, Correct MDate' . '<br>';
perform_test();

// Test insert with correct parameters, and begin logged in.
$_REQUEST['Function'] = 'Insert';
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 5: Correct Parameters Insert' . '<br>';
perform_test();

// Test insert again, as duplicate entry.
$_REQUEST['Function'] = 'Insert';
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 6: Duplicate Insert' . '<br>';
perform_test();

// Test deletion with correct parameters.
$_REQUEST['Function'] = 'Delete';
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 7: Correct Parameters Delete' . '<br>';
perform_test();

// Test deletion again, of non existing entry.
$_REQUEST['Function'] = 'Delete';
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 8: Duplicate Delete' . '<br>';
perform_test();

// Test incorrect date format.
$_REQUEST['Function'] = 'Insert';
$_REQUEST['MDate'] = 'January 10, 2001';
echo 'Test 9: Incorrect Date Format' . '<br>';
perform_test();

// Test date that does not exist.
$_REQUEST['Function'] = 'Insert';
$_REQUEST['MDate'] = '2001-01-09';
echo 'Test 10: Meeting Does Not Exist' . '<br>';
perform_test();
?>