<?php
/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test() {
    include('../api/get_availability_date.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

// Test without any parameters.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test with only OnlyActive flag specified.
$_REQUEST['OnlyActive'] = '1';
echo 'Test 2: OnlyActive Parameter Only Specified' . '<br>';
perform_test();

// Test with only MDate specified.
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 3: Only MDate Specified' . '<br>';
perform_test();

// Test with incorrect date format.
$_REQUEST['MDate'] = '20010110';
echo 'Test 4: Incorrect Date Format' . '<br>';
perform_test();

// Test with a date for a meeting that does not exist.
$_REQUEST['MDate'] = '2001-01-09';
echo 'Test 5: Meeting Does Not Exist' . '<br>';
perform_test();

// Test with the OnlyActive flag = 1.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['OnlyActive'] = '1';
echo 'Test 6: OnlyActive Flag = 1' . '<br>';
perform_test();

// Test with the OnlyActive flag = 0.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['OnlyActive'] = '0';
echo 'Test 7: OnlyActive Flag = 0' . '<br>';
perform_test();
?>