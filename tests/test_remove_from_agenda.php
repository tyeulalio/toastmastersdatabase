<?php
include('../api/_global.php');

/**
 * Call our API. Reset the header and the REQUEST variables for the next test.
 */
function perform_test()
{
    include('../api/remove_from_agenda.php');
    echo '<br><br>';
    header_remove();
    $_REQUEST = array();
}

/**
 * Reinsert our missing data.
 *
 * @param $c mysqli Connection object to our database.
 */
function reinsert_missing($c)
{
    if ($c->query("INSERT INTO SCHEDULE () VALUES (1, 17, 0, '2001: A Speech Odyssey'), 
                        (1, 7, 69, 'Greg Yuen')")) {
        echo 'Items Reinserted...<br><br>';
    } else die ('Error: ' . $c->error);
}

// Test without any parameters.
echo 'Test 1: No Parameters' . '<br>';
perform_test();

// Test with only MDate specified.
$_REQUEST['MDate'] = '2001-01-10';
echo 'Test 2: Only MDate Specified' . '<br>';
perform_test();

// Test with MDate and empty RemoveItemList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['RemoveItemList'] = '';
echo 'Test 3: Correct MDate, Empty RemoveItemList' . '<br>';
perform_test();

// Test with MDate and an incorrect item in RemoveItemList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['RemoveItemList'] = 'Meeting Theme,Speaker1,LOOKATME';
echo 'Test 4: Incorrect Item in RemoveItemList' . '<br>';
perform_test();
reinsert_missing($conn);

// Test with correct MDate and RemoveItemList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['RemoveItemList'] = 'Meeting Theme,Speaker1';
echo 'Test 6: Happy Path, Correct MDate and RemoveItemList' . '<br>';
perform_test();
reinsert_missing($conn);

// Test with incorrect date format.
$_REQUEST['MDate'] = '20010110';
$_REQUEST['RemoveItemList'] = 'Meeting Theme,Speaker1';
echo 'Test 7: Incorrect Date Format' . '<br>';
perform_test();

// Test with a date for a meeting that does not exist.
$_REQUEST['MDate'] = '2001-01-09';
$_REQUEST['RemoveItemList'] = 'Meeting Theme,Speaker1';
echo 'Test 8: Meeting Does Not Exist' . '<br>';
perform_test();

// Test with MDate and empty item in RemoveItemList.
$_REQUEST['MDate'] = '2001-01-10';
$_REQUEST['RemoveItemList'] = 'Meeting Theme,Speaker1,';
echo 'Test 9: Extra Comma in RemoveItemList' . '<br>';
perform_test();
reinsert_missing($conn);
?>