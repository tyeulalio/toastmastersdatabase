<?php
/*** 

Add a new member to the roster table.

To use this code, set the following:
	Function=add
	FirstName=[new member's fisrt name]
	LastName=[new member's last name]

The database will automatically assign the newly auto-incremented TID.

***/


include("_global.php");	// starts session, returns connector to database $conn 

// get TID and input
$tid = $session_tid * 1;
$function = $_REQUEST['Function'];
$FirstName=addslashes($_REQUEST['FirstName']);
$LastName=addslashes($_REQUEST['LastName']);

// keep track of SQL query status
$affected_rows = 0;


// Define the default values of our response JSON.
$response = array_fill_keys(array('msg', 'TID', 'Time'), '');
$response['TID'] = $usrTid;

// new members must input FirstName and LastName
if($tid>0 && $function=='add' && $FirstName>'' && $LastName>''){

		/***

		We'll make the requests for all of the input using the t_attributes stored in the TOASTMASTER_ATTRIBUTES table.
		Make a request using each t_attribute name (stored as $attribute) and save input as $fieldText.
		Store these pairs in the $memberInfo array with key => $attribute and value => $fieldText.

		***/

		// retrieve the t_attributes list from the toastmaster_attributes table
		$sql = "SELECT * FROM TOASTMASTER_ATTRIBUTES";
		$sqlQuery = $conn->query($sql);


		$memberInfo = Array(); // array to hold member info
		// populate the array with query feedback
		while ($row = $sqlQuery->fetch_assoc()){
			
			$t_attribute = $row[T_ATTRIBUTE];

			// request the t_attribute
			$text = addslashes($_REQUEST[$t_attribute]);

			$memberInfo[$t_attribute] = $text;
		}


		/***

		Start with inserting the FirstName of the new member.
		This sql query will return the auto-incremented TID for the new member.
		Once we obtain this TID, we can insert the rest of the fields with this associated TID.

		***/
		
		// insert the FirstName to get the newly generated TID
		$sql = "INSERT INTO ROSTER (T_ATTRIBUTE, TEXT) VALUES('FirstName', '$FirstName')";
		$sqlQuery = $conn->query($sql);

		
		$newTid = mysqli_insert_id($conn);
		$affected_rows = mysqli_affected_rows($conn);


		if($affected_rows==0) { $response['msg'] = "Error: new member was not added."; }

		else{
			// New member's first name has been added successfully and TID stored as $newTid

			$sql = '';
			$count = 1; // keeps track of number of fields to insert for new member


			// create the SQL statement for all of the inserts
			// we're inserting the rest of the fields in one query
			$sql = "INSERT INTO ROSTER (TID, T_ATTRIBUTE, TEXT) VALUES "; 
			foreach ($memberInfo as $key => $value){ // loop through entire $memberInfo array that we created earlier
				if ($value > '' && $key != 'FirstName'){ // we already added FirstName attribute
					$sql .= "('$newTid', '$key', '$value'), ";
					$count++;
				}
			}
			// replace last comma with a semi-colon in sql statement
			$sql = rtrim($sql, ", ");
			$sql .= ';';



			// perform the sql query
			$sqlQuery = $conn->query($sql);
			$affected_rows = mysqli_affected_rows($conn); 
			$affected_rows++; // add 1 for the FirstName that was already inserted earlier
			
			if ($affected_rows == $count){ // all fields were inserted 
				$response['msg'] .= "Success";
				$response['Status'] = "'$affected_rows' field(s) inserted into ROSTER table.";
			}
			else{
				$notInserted = $count - $affected_rows;
				$response['msg'] .= "Error: '$notInserted' field(s) not inserted. '$affected_rows' field(s) inserted successfully.";
			}

		}
}

// if first and last names are not entered
else if (!$FirstName>'' || !$LastName>''){
	$response['msg'] = "Error: no new member added. New members must enter a first name and last name. (e.g. ?FirstName=John&LastName=Snow)";
}

// must be logged in as a member to add a new member. Looks at session tid.
else{
	$response['msg'] = "Error: no new member added. Please log in to add a new member.";
}


// select all fields of our newly added member to display as output
$sql = "SELECT * FROM ROSTER WHERE TID='$newTid'";
$sqlQuery = $conn->query($sql);


// store all member information into an array for output
while($row = $sqlQuery->fetch_assoc()){
	$t_attribute = $row[T_ATTRIBUTE];
	$text = $row[TEXT];

	// replace password with asterisks
	if ($t_attribute == 'Password' && $text > ''){
		$text = str_repeat("*", strlen($text));
	}

	$response[$t_attribute] = $text;
}
$response['TID'] = $newTid;

// output the newly added member data
$response['Time'] = date('F j, Y, g:i A');

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
printf("<pre>%s</pre>", $json);

?>

