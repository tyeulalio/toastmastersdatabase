<?php

include("_global.php");	// starts session, returns connector to database $conn 

$usrTid = $session_tid; // grabs the user's tid

// $super and $inputTid allow a super user to update other member's info
$super = $_REQUEST['super']; // enter this as 1234 to get super powers
$inputTid = $_REQUEST['TID'];

$function = $_REQUEST['Function'];
$field=$_REQUEST['Field'];
$fieldNew=addslashes($_REQUEST['FieldNew']);
$fieldOld=addslashes($_REQUEST['FieldOld']);

$affected_rows = 0;
$msg='';
$tid = 0;

// Define the default values of our response JSON.
$response = array_fill_keys(array('msg', 'TID', 'Time', 'Status'), '');
$response['TID'] = $usrTid;
$response['Status'] = '';

// check if the super user is trying to update a member's info
if ($super = 1234 && $inputTid > ''){
	$tid = $inputTid;
}
else{
	$tid = $usrTid;
}


if($tid>0 && $function=='update' && $field>'' && $fieldOld != $fieldNew){
	// check whether the field exists as a T_ATTRIBUTE for TID
	$sql = "SELECT * FROM ROSTER WHERE TID='$tid' AND T_ATTRIBUTE='$field'";
	$sqlQuery = $conn->query($sql);

	// check if query returned anything
	$affected_rows = $sqlQuery->num_rows;
	if($affected_rows==0) { $response['Status'] = "TID '$tid' does not have an existing '$field' field. "; }
	else $response['Status'] .= "TID '$tid' has an existing '$field' field. ";

	// if T_ATTRIBUTE.'$field' exists for TID, then update it
	if ($affected_rows>0){
		$sql = "UPDATE ROSTER SET TEXT='$fieldNew' WHERE TID='$tid' AND T_ATTRIBUTE='$field' AND TEXT='$fieldOld'";
	}
	// else insert the column
	else{
		$sql = "INSERT INTO ROSTER VALUES('$tid','$field','$fieldNew')";
	}

	// check whether query worked
	if($conn->query($sql) == 0) { 
		$response['msg'] .= "There was a conflict. No values were updated. "; 
	}
	else {
		$response['Status'] .= "$field was updated from '$fieldOld' to '$fieldNew'. ";
		$response['msg'] = "Success";
	}
}

// if fieldOld == fieldNew
else if($function=='update' && $tid>0 && $field>'')
{
	$response['msg'] .= "Nothing to update. FieldOld and FiledNew are the same. ";
}

// if field wasn't input
else if($function=='update' && $tid>0)
{
	$response['msg'] .= "Nothing to update. Field has not been specified. ";
}

// allow for marking a member for deletion
else if($function == 'delete'){
	// assume that members only mark mistakes for deletion
	// all actual members will never be deleted, only inactivated
	$tid = $inputTid;

	// we will utilize the Active filed of the member to mark them for deletion
	// check that the member exists
	$sql = "SELECT * FROM ROSTER WHERE TID='$tid'";
	$sqlQuery = $conn->query($sql);

	// check if query returned anything
	$affected_rows = $sqlQuery->num_rows;
	if($affected_rows==0) { $response['msg'] = "Member '$tid' does not exist "; }
	else $response['Status'] .= "Member '$tid' exists. ";

	// if the member exists, check whether the Active field exists
	$sql = "SELECT * FROM ROSTER WHERE TID='$tid' && T_ATTRIBUTE='Active'";
	$sqlQuery = $conn->query($sql);
	// check if query returned anything
	$affected_rows = $sqlQuery->num_rows;

	if (affected_rows > 0){
		$sql = "UPDATE ROSTER SET TEXT=2 WHERE TID='$tid' AND T_ATTRIBUTE='Active'";
	}
	else{
		$sql = "INSERT INTO ROSTER VALUES('$tid','Active', '2')";
	}

	// check whether query worked
	if($conn->query($sql) == 0) { 
		$response['msg'] .= "There was a conflict. Member was not marked for deletion. "; 
	}
	else {
		$response['Status'] .= "Member '$TID' has been marked for deletion. ";
		$response['msg'] = "Success";
	}
}


// store the member info for output
if($tid>0)
{
	$sql = "SELECT * FROM ROSTER WHERE TID='$tid'";

	$sqlQuery = $conn->query($sql);

	while ($row = $sqlQuery->fetch_assoc()){
			$t_attribute = $row[T_ATTRIBUTE];
			$text = $row[TEXT];

			// replace password with asterisks
			if ($t_attribute == 'Password' && $text > ''){
				$text = str_repeat("*", strlen($text));
			}

			$response[$t_attribute] = $text;
	}
}
else $response['msg'] .= "Member TID not specified. e.g., ?tid=164";

$rightNow = date('F j, Y, g:i A');
$response['Time'] = $rightNow;


header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
printf("<pre>%s</pre>", $json);

?>

