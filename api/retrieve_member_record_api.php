<?php

/***

	Allows you to retrieve information on a member.
	
	A member must be logged in and can only view their own member info.
	For testing purposes, specifying tid and super=1 in input will allow you to view any member's input.

***/


include("_global.php");	// starts session, returns connector to database $conn 

$usrTid = $session_tid * 1; // make sure member is logged in to retrieve info

// Remove the next two lines when testing is complete!!
$super = addslashes($_REQUEST['super']); // allows you to view anyone's member info
$retrieveTid = addslashes($_REQUEST['TID']); // tid of member whose info we're requesting

// Define the default values of our response JSON.
$response = array_fill_keys(array('msg', 'TID'), '');
$response['TID'] = $usrTid;

$tid = '';
$msg = ''; // used for output message

// assign the tid appropriately to the input
if ($super == 1234 && $retrieveTid > ''){ // users with super power can check any member's info
	$tid = $retrieveTid;
}
else{ // regular members can only see their own info
	$tid = $usrTid;
}


// tid must be greater than 1 to continue with query
if ($tid > 0){
	// grab member info associated with retrieveTid
	$sql="SELECT * FROM ROSTER WHERE TID='$tid'";

	$sqlQuery = $conn->query($sql);

	// check if query returned anything
	$affected_rows = $sqlQuery->num_rows;
	$memberInfo = Array();

	// if query didn't return anything, then TID does not exist
	if ($affected_rows == 0) {
		$response['msg'] = "Error: TID does not exist.";
	}

	else {
		// TID exists, so store all member information into an array for output
		while ($row = $sqlQuery->fetch_assoc()){
			$t_attribute = $row[T_ATTRIBUTE];
			$text = $row[TEXT];

			// replace password with asterisks
			if ($t_attribute == 'Password' && $text > ''){
				$text = '---';
			}

			$response[$t_attribute] = $text;
		}

		$response['msg'] = "Success";
	}

}

$rightNow = date('F j, Y, g:i A');
$response['Time'] = $rightNow;

// Display the output
header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
printf("<pre>%s</pre>", $json);

?>