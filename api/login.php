<?php
/**
 * Login and define our TID variable. To update anything our database, we require that our TID variable be non-zero.
 *
 * Requested Variables: TID -> ID of the user currently trying to login.
 *                      Password -> Entered password to perform login with.
 *
 * Updated Session:     TID -> ID of the user associated with the session.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      TID -> Integer ID of the user with the current session.
 *                      Active -> Integer representing 0 for non-active, and 1 for active.
 *                      City -> String of city associated with the user.
 *                      Email -> String of email associated with the user.
 *                      FirstName -> String of first name of the user.
 *                      HomePhone -> String of home phone number associated with the user.
 *                      LastName -> String of last name of the user.
 *                      State -> String of state associated with the user.
 *                      StreetAddress -> String of address associated with the user.
 *                      Title -> String of title associated with the user.
 *                      WorkPhone -> String of work phone number associated with the user.
 *                      Zipcode -> String of zip code associated with the user.
 */
include("_global.php");
$stmt = $conn->stmt_init();

// Grab our TID and password. This can be from POST, GET or cookies.
$tid_in = $_REQUEST["TID"] * 1;
$pass_in = addslashes($_REQUEST["Password"]);

// Define the default values of our response JSON.
$response = array_fill_keys(array('msg', 'TID', 'Active', 'City', 'Email', 'FirstName', 'HomePhone', 'LastName',
    'State', 'StreetAddress', 'Title', 'WorkPhone', 'Zipcode'), '');
$response['TID'] = 0;
$response['Active'] = -1;

if ($tid_in > 0 && $pass_in != '') {
    // Search for our user with the given credentials.
    $stmt = $conn->prepare("SELECT TID FROM ROSTER WHERE TID = ? AND T_ATTRIBUTE = 'Password' AND TEXT = ?");
    $stmt->bind_param('is', $tid_in, $pass_in);
    $stmt->execute();
    $result = $stmt->get_result();

    // Set our response array, using our result tuple.
    if ($u = $result->fetch_assoc()["TID"]) {
        $sql_user = "SELECT T_ATTRIBUTE, TEXT FROM ROSTER WHERE TID = $u AND T_ATTRIBUTE <> 'Password'";
        $result_user = $conn->query($sql_user);
        while ($v = $result_user->fetch_assoc()) {
            $response[$v['T_ATTRIBUTE']] = $v['TEXT'];
        }

        // Our session now has a user associated with it.
        $response['TID'] = $u;
        $_SESSION['TID'] = $response['TID'];
        $response['msg'] = 'Success';

    } else $response['msg'] = 'User credentials incorrect.';
} elseif ($tid_in > 0 && $pass_in === '') {
    // Search for our user without credentials.
    $stmt = $conn->prepare("SELECT DISTINCT TID FROM ROSTER WHERE TID = ? AND (SELECT NOT EXISTS (SELECT 1 
                                   FROM ROSTER WHERE TID = ? AND T_ATTRIBUTE = 'Password'))");
    $stmt->bind_param('ss', $tid_in, $tid_in);
    $stmt->execute();
    $result = $stmt->get_result();

    // Set our response array, using our result tuple.
    if ($u = $result->fetch_assoc()["TID"]) {
        $sql_user = "SELECT T_ATTRIBUTE, TEXT FROM ROSTER WHERE TID = $u AND T_ATTRIBUTE <> 'Password'";
        $result_user = $conn->query($sql_user);
        while ($v = $result_user->fetch_assoc()) {
            $response[$v['T_ATTRIBUTE']] = $v['TEXT'];
        }

        // Our session now has a user associated with it.
        $response['TID'] = $u;
        $_SESSION['TID'] = $response['TID'];
        $response['msg'] = 'Success';
    } else $response['msg'] = 'User credentials incorrect.';

} else $response['msg'] = 'TID or Password has not been specified.';
$stmt->close();

// Set our response.
header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;
?>