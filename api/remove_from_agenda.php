<?php
/**
 * Remove the given items for a meeting agenda on the given date. The input for these items are in a single comma
 * separated list of the types of text to remove. Example Usage:
 *
 * POST('MDate', 2001-01-10)
 * POST('RemoveItemList', 'Location of Meeting,Presiding Officer')
 *
 * The return JSON is the same as the get_agenda.php function. There are four comma separated lists here: the item list,
 * item number list, type list, and the text list. The elements for all lists map to the other lists through indices.
 *
 * Requested Variables: MDate -> The date to update the agenda for. Input must be in the format YYYY-MM-DD.
 *                      RemoveItemList -> The name of the duty or attribute for a text to delete, as a comma separated
 *                      list.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      MDate -> Meeting date specified by the user.
 *                      MID -> MID of the meeting date specified by the user.
 *                      ItemList -> Either the name of the duty or the name of the attribute for an text, as a
 *                      comma separated list.
 *                      ItemNumberList -> The item number of the duty or attribute for a given text, as a comma
 *                      separated list.
 *                      TypeList -> The type of item corresponding the given text. In space ['ATTRIBUTE', 'DUTY'].
 *                      TextList -> The text corresponding to the duty/attribute and the item number, as a comma
 *                      separated list.
 */
include('_global.php');

// Grab our input date and list to remove.
$date_in = $_REQUEST['MDate'];
$remove_item_in = $_REQUEST['RemoveItemList'];

// Verify that our date is in the correct format. Magic! Woah! Das ist sehr lang!
$reg_date = "/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012]
)\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|
((16|[2468][048]|[3579][26])00))\-02\-29))$/";

// Define the default values for our response array.
$response = array_fill_keys(array('msg', 'MDate', 'MID', 'ItemList', 'ItemNumberList', 'TypeList', 'TextList'), '');
$response['MDate'] = $date_in;

// Separated our list by commas. Ignore the escaped commas.
$remove_item_list = preg_split('~(?<!\\\)' . preg_quote(',', '~') . '~', $remove_item_in);

/**
 * Search and put our agenda for the given meeting into our response array $r.
 *
 * @param $c mysqli Connection to our database.
 * @param $m int MID corresponding to the meeting to grab the agenda for.
 * @param $r array Response array to store meeting agenda in.
 * @return array Our response array with our agenda.
 */
if (!function_exists('grab_agenda')) {
    function grab_agenda($c, $m, $r)
    {
        // Grab all the items an agenda requires. We order this the specified agenda order.
        $sql_select_items = "SELECT ITEM, ITEM_NUMBER, AType FROM AGENDA ORDER BY AOrder ASC";
        $result_select_items = $c->query($sql_select_items);
        while ($u = $result_select_items->fetch_assoc()) {
            $r['ItemList'] .= addslashes($u['ITEM']) . ",";
            $r['ItemNumberList'] .= $u['ITEM_NUMBER'] . ",";
            $r['TypeList'] .= $u['AType'] . ",";
            $item_number = $u['ITEM_NUMBER'];

            // Search for this item at the given meeting date. If we have nothing, insert only a comma.
            $sql_select_text = "SELECT TEXT FROM SCHEDULE WHERE MID = $m AND ITEM_NUMBER = $item_number";
            if ($w = $c->query($sql_select_text)->fetch_assoc()) {
                $r['TextList'] .= addslashes($w['TEXT']) . ",";
            } else $r['TextList'] .= ",";
        }

        // If any list is empty, then our agenda retrieval was not successful.
        if ($r['ItemList'] == '' || $r['ItemNumberList'] == '' || $r['TypeList'] == '' || $r['TextList'] == '') {
            $r['msg'] .= 'No agenda items found';
        }

        // Remove trailing commas in our lists.
        foreach (array('ItemList', 'ItemNumberList', 'TypeList', 'TextList') as $v) {
            $r[$v] = rtrim($r[$v], ',');
        }

        // Return our response array.
        return $r;
    }
}

if (!preg_match($reg_date, $date_in)) {
    $response['msg'] = "Given date not in correct format.";

} else {
    $stmt = $conn->prepare("SELECT MID FROM MEETING_DATES WHERE DATE(MDate) = ?");
    $stmt->bind_param('s', $date_in);
    $stmt->execute();
    $result = $stmt->get_result();

    // Convert our date into a MID.
    if ($u = $result->fetch_assoc()) {
        $sql_date_to_mid = "SELECT MID FROM MEETING_DATES WHERE DATE(MDate) = $date_in";
        $mid = $u['MID'];
        $response['MID'] = $mid;

        // For each item, find the item number.
        foreach ($remove_item_list as $p) {
            $sql_select_item_num = "SELECT ITEM_NUMBER FROM AGENDA WHERE ITEM = '$p'";

            // If we have an entry here, delete it.
            if ($a = $conn->query($sql_select_item_num)->fetch_assoc()) {
                $item_num = $a['ITEM_NUMBER'];
                $sql_remove = "DELETE FROM SCHEDULE WHERE ITEM_NUMBER = $item_num AND MID = $mid";

                if ($conn->query($sql_remove) === TRUE) {
                    $response['msg'] = 'Success';
                } else die ("Error: " . $conn->error);
            } else {
                $response['msg'] .= "Item " . $p . " does not exist in AGENDA. ";
                break;
            }
        }

        // Grab our agenda and put this in our response array.
        $response = grab_agenda($conn, $mid, $response);

    } else $response['msg'] = 'There is no meeting entry on the date ' . $date_in . ".";
}

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;

?>