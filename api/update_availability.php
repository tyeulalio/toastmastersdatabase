<?php
/**
 * Delete or create a new entry for our UNAVAILABLE table. This just marks a user as available by removing an existing
 * tuple, or marks a user as unavailable by creating a new tuple.
 *
 * Requested Variables: Function -> Decision to delete or insert. Exists in space ['Delete', 'Insert'].
 *                      MDate -> Date to delete or insert into our UNAVAILABLE table, in format YYYY-MM-DD.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      TID -> Integer ID of the user with the current session.
 *                      MDate -> Meeting date of the meeting removed/updated.
 *                      MID -> Meeting ID of the meeting removed/updated.
 */
include('_global.php');

// Grab our function, date, and TID.
$function_in = $_REQUEST['Function'];
$date_in = $_REQUEST['MDate'];
$tid = $session_tid * 1;

// Define the default values of our response JSON.
$response = array_fill_keys(array('msg', 'TID', 'MDate', 'MID'), '');
$response['MDate'] = $date_in;
$response['TID'] = $tid;

/**
 * Converts a given date D into an MID from our MEETING_DATES table.
 *
 * @param $c mysqli Connection object obtained through login.
 * @param $d string Date field.
 * @return int 0 if there exists no date here. The corresponding MID otherwise.
 */
if (!function_exists('date_to_mid')) {
    function date_to_mid($c, $d)
    {
        $stmt = $c->prepare("SELECT MID FROM MEETING_DATES WHERE DATE(MDate) = ?");
        $stmt->bind_param('s', $d);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($u = $result->fetch_assoc()) {
            return $u['MID'];
        } else return 0;
    }
}

/**
 * Check if our MID is valid and if an entry exists in the UNAVAILABLE table with the given TID.
 *
 * @param $c mysqli Connection object obtained through login.
 * @param $mid int MID obtained through date_to_mid function.
 * @param $t string TID obtained through login.
 * @return string 'Invalid MID' for MID = 0, 'Entry exists' if an entry exists, and 'Entry does not exist' otherwise.
 */
if (!function_exists('does_date_exist')) {
    function does_date_exist($c, $mid, $t)
    {
        // Verify that our date exists in the MEETING_DATES table.
        if ($mid == 0) {
            return 'Invalid MID';

        } else {
            // Check if the given user has an entry for this date.
            $sql_unav_entry = "SELECT MID FROM UNAVAILABLE WHERE MID = $mid AND TID = $t";
            if ($u = $c->query($sql_unav_entry)->fetch_assoc()) {
                return 'Entry exists.';
            } else return 'Entry does not exist.';
        }
    }
}

// Verify that we have login information (i.e. TID is != 0).
if ($tid > 0) {
    if ($function_in == 'Delete') {
        $mid = date_to_mid($conn, $date_in);
        $r = does_date_exist($conn, $mid, $tid);
        $response['MID'] = $mid;

        // Verify that we have something to delete.
        if ($r == 'Invalid MID') {
            $response['msg'] = 'There is no meeting entry on the date ' . $date_in . '.';
        } elseif ($r == 'Entry does not exist.') {
            $response['msg'] = 'No entry to delete with MID = ' . $mid . ' AND TID = ' . $tid . '.';

        } else {
            // If we do, perform the deletion.
            $sql_delete = "DELETE FROM UNAVAILABLE WHERE MID = $mid AND TID = $tid";
            if ($conn->query($sql_delete) === TRUE) {
                $response['msg'] = 'Success';

            } else die('Error: ' . $conn->error . "<br>");
        }
    } elseif ($function_in == 'Insert') {
        $mid = date_to_mid($conn, $date_in);
        $r = does_date_exist($conn, $mid, $tid);
        $response['MID'] = $mid;

        // Verify that we can insert something.
        if ($r == 'Invalid MID') {
            $response['msg'] = 'There is no meeting entry on the date ' . $date_in . '.';
        } elseif ($r == 'Entry exists.') {
            $response['msg'] = 'Entry already exists with MID = ' . $mid . ' AND TID = ' . $tid . '.';

        } else {
            // If we do, perform the insertion.
            $sql_insert = "INSERT INTO UNAVAILABLE (MID, TID) VALUES ($mid, $tid)";
            if ($conn->query($sql_insert) === TRUE) {
                $response['msg'] = 'Success';

            } else die('Error: ' . $conn->error . "<br>");
        }
    } else {
        // Invalid function. No actions performed.
        $response['msg'] = "Function not in space ['Delete', 'Insert'].";
    }
} else $response['msg'] = "No available login info. TID is empty.";

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;
?>