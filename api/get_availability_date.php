<?php
/**
 * Retrieve the availability table for all members on the specified date. If we are available, this is paired
 * with '1'. Otherwise,an unavailable date is paired with '0'.
 *
 * Requested Variables: MDate -> The date to retrieve availability for. Input must be in the format YYYY-MM-DD.
 *                      OnlyActive -> If specified, 1 indicates that we only display availability for active members.
 *                      0 indicates that we display availability for all members. We default to 1 here.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      MDate -> Date of the availability attached.
 *                      MID -> MID of the given MDate.
 *                      TIDList -> Comma separated list of members corresponding to the meeting availability.
 *                      AvailabilityList -> Comma separated list of availabilities. Indices map to the TIDList.
 */
include('_global.php');

// Grab our input date, and our active flag.
$date_in = $_REQUEST['MDate'];
$active_in = $_REQUEST['OnlyActive'];

// Verify that our date is in the correct format. Magic! Woah! Das ist sehr lang!
$reg_date = "/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012]
)\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|
((16|[2468][048]|[3579][26])00))\-02\-29))$/";

// Define the default values for our response array.
$response = array_fill_keys(array('msg', 'MDate', 'MID', 'TIDList', 'AvailabilityList'), '');
$response['MDate'] = $date_in;

if (!preg_match($reg_date, $date_in)) {
    $response['msg'] = "Given date not in correct format.";
} elseif ($active_in != '1' && $active_in != '0' && $active_in != '') {
    $response['msg'] = "OnlyActive not in space [1, 0]";

} else {
    // Convert our date into an MID.
    $mid = '';
    $sql_date_to_mid = "SELECT MID FROM MEETING_DATES WHERE MDate = '$date_in'";
    if ($u = $conn->query($sql_date_to_mid)->fetch_assoc()) {
        $mid = $u['MID'];
        $response['MID']  = $mid;

        // Construct our user query. If we only want active users, specify so.
        $sql_select_users = "SELECT DISTINCT TID FROM ROSTER";
        if ($active_in == '' || $active_in == '1') {
            $sql_select_users .= " WHERE T_ATTRIBUTE = 'Active' AND TEXT = '1'";
        }
        $result_select_users = $conn->query($sql_select_users);

        // Specify our error message.
        if ($result_select_users->num_rows > 0) {
            $response['msg'] = 'Success';
        }

        // Iterate through our users.
        while ($v = $result_select_users->fetch_assoc()) {
            $tid = $v['TID'];

            // Append our user to the list.
            $response['TIDList'] .= $tid . ",";

            // See if this user is unavailable at this date.
            $sql_is_available = "SELECT 1 FROM UNAVAILABLE WHERE MID = $mid AND TID = $tid";
            if($conn->query($sql_is_available)->num_rows == 1) {
                $response['AvailabilityList'] .= '0,';
            }
            $response['AvailabilityList'] .= '1,';
        }

        // Remove the trailing commas from our lists.
        $response['TIDList'] = rtrim($response['TIDList'], ',');
        $response['AvailabilityList'] = rtrim($response['AvailabilityList'], ',');

    } else $response['msg'] = 'There is no meeting entry on the date ' . $date_in . ".";
}

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;
?>