<?php
/**
 * Retrieve the availability table for all meetings between the specified dates.  If we are available, this is paired
 * with '1'. Otherwise,an unavailable date is paired with '0'.
 *
 * Requested Variables: DateBegin -> Specify the meeting date of availabilities to begin searching with. Input must be
 *                      in the format YYYY-MM-DD.
 *                      DateEnd -> Specify the meeting date of availabilities to end searching with. Input must be
 *                      in the format YYYY-MM-DD.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      TID -> The user attached to the current session.
 *                      MDateList -> Comma separated list of dates corresponding to the current user.
 *                      AvailabilityList -> Comma separated list of availabilities. Indices map to the MDateList.
 */
include('_global.php');

// Grab our dates and TID.
$begin_date_in = $_REQUEST['DateBegin'];
$end_date_in = $_REQUEST['DateEnd'];
$tid = $session_tid * 1;

// Verify that our dates are in the correct format. Magic! Woah! Das ist sehr lang!
$reg_date = "/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012]
)\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|
((16|[2468][048]|[3579][26])00))\-02\-29))$/";

// Define the default values for our response array.
$response = array_fill_keys(array('msg', 'TID', 'MDateList', 'AvailabilityList'), '');
$response['TID'] = $tid;

if ($tid == 0) {
    $response['msg'] = "No available login info. TID is empty.";
} elseif (!preg_match($reg_date, $begin_date_in) || !preg_match($reg_date, $end_date_in)) {
    $response['msg'] = "Dates not in the correct format.";
} elseif ($begin_date_in == '' && $end_date_in == '') {
    $response['msg'] = "BeginDate and EndDate not specified.";

} else {
    // User must be logged in and specify two dates.
    $sql_mid_list = "SELECT MID, MDate FROM MEETING_DATES WHERE MDate BETWEEN '$begin_date_in' AND '$end_date_in'";
    $result_mid_list = $conn->query($sql_mid_list);

    // Specify our error message.
    if ($result_mid_list->num_rows > 0) {
        $response['msg'] = 'Success';
    } else {
        $response['msg'] = 'No meetings found between ' . $begin_date_in . " and " . $end_date_in;
    }

    // Search for MIDs between the two dates.
    while ($mid_list = $result_mid_list->fetch_assoc()) {
        $mdate = $mid_list['MDate'];
        $mid = $mid_list['MID'];

        // Append the current date to the date list.
        $response['MDateList'] .= $mdate . ",";

        // If we have a result, append an 1. Otherwise, append a 0.
        $sql_is_unavailable = "SELECT 1 FROM UNAVAILABLE WHERE TID = $tid AND MID = $mid";
        if ($conn->query($sql_is_unavailable)->num_rows == 1) {
            $response['AvailabilityList'] .= '0,';
        }
        $response['AvailabilityList'] .= '1,';
    }

    // Remove the trailing commas from our lists.
    $response['MDateList'] = rtrim($response['MDateList'], ',');
    $response['AvailabilityList'] = rtrim($response['AvailabilityList'], ',');
}

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;
?>