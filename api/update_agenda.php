<?php
/**
 * Update the given items for a meeting agenda on the given date. The input for these items are two comma separated
 * lists. One that lists the type of text, and one for the text to update. To update a duty, enter the user's TID. To
 * update an attribute, enter the desired text. Example Usage:
 *
 * POST('MDate', 2001-01-10)
 * POST('NewItemList', 'Location of Meeting,Presiding Officer')
 * POST('NewEntryList', 'Some Hotel,18')
 *
 * The return JSON is the same as the get_agenda.php function. There are four comma separated lists here: the item list,
 * item number list, type list, and the text list. The elements for all lists map to the other lists through indices.
 *
 * Requested Variables: MDate -> The date to update the agenda for. Input must be in the format YYYY-MM-DD.
 *                      NewItemList -> The name of the duty or attribute for a given text, as a comma separated list.
 *                      NewEntryList -> The text corresponding to the duty/attribute as a comma separated list.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      MDate -> Meeting date specified by the user.
 *                      MID -> MID of the meeting date specified by the user.
 *                      ItemList -> Either the name of the duty or the name of the attribute for an text, as a
 *                      comma separated list.
 *                      ItemNumberList -> The item number of the duty or attribute for a given text, as a comma
 *                      separated list.
 *                      TypeList -> The type of item corresponding the given text. In space ['ATTRIBUTE', 'DUTY'].
 *                      TextList -> The text corresponding to the duty/attribute and the item number, as a comma
 *                      separated list.
 */
include('_global.php');

// Grab our input date and texts to update.
$date_in = $_REQUEST['MDate'];
$new_item_in = $_REQUEST['NewItemList'];
$new_entry_in = $_REQUEST['NewEntryList'];

// Verify that our date is in the correct format. Magic! Woah! Das ist sehr lang!
$reg_date = "/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012]
)\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|
((16|[2468][048]|[3579][26])00))\-02\-29))$/";

// Define the default values for our response array.
$response = array_fill_keys(array('msg', 'MDate', 'MID', 'ItemList', 'ItemNumberList', 'TypeList', 'TextList'), '');
$response['MDate'] = $date_in;

// Separated our lists by commas. Ignore the escaped commas.
$new_item_list = preg_split('~(?<!\\\)' . preg_quote(',', '~') . '~', $new_item_in);
$new_entry_list = preg_split('~(?<!\\\)' . preg_quote(',', '~') . '~', $new_entry_in);

/**
 * Search and put our agenda for the given meeting into our response array $r.
 *
 * @param $c mysqli Connection to our database.
 * @param $m int MID corresponding to the meeting to grab the agenda for.
 * @param $r array Response array to store meeting agenda in.
 * @return array Our response array with our agenda.
 */
if (!function_exists('grab_agenda')) {
    function grab_agenda($c, $m, $r)
    {
        // Grab all the items an agenda requires. We order this the specified agenda order.
        $sql_select_items = "SELECT ITEM, ITEM_NUMBER, AType FROM AGENDA ORDER BY AOrder ASC";
        $result_select_items = $c->query($sql_select_items);
        while ($u = $result_select_items->fetch_assoc()) {
            $r['ItemList'] .= addslashes($u['ITEM']) . ",";
            $r['ItemNumberList'] .= $u['ITEM_NUMBER'] . ",";
            $r['TypeList'] .= $u['AType'] . ",";
            $item_number = $u['ITEM_NUMBER'];

            // Search for this item at the given meeting date. If we have nothing, insert only a comma.
            $sql_select_text = "SELECT TEXT FROM SCHEDULE WHERE MID = $m AND ITEM_NUMBER = $item_number";
            if ($w = $c->query($sql_select_text)->fetch_assoc()) {
                $r['TextList'] .= addslashes($w['TEXT']) . ",";
            } else $r['TextList'] .= ",";
        }

        // If any list is empty, then our agenda retrieval was not successful.
        if ($r['ItemList'] == '' || $r['ItemNumberList'] == '' || $r['TypeList'] == '' || $r['TextList'] == '') {
            $r['msg'] .= 'No agenda items found';
        }

        // Remove trailing commas in our lists.
        foreach (array('ItemList', 'ItemNumberList', 'TypeList', 'TextList') as $v) {
            $r[$v] = rtrim($r[$v], ',');
        }

        // Return our response array.
        return $r;
    }
}

/**
 * Given the TID of a member, return their first and last name as: 'FIRSTNAME LASTNAME'.
 *
 * @param $c mysqli Connection to our database.
 * @param $t int TID of the user to return the name for.
 * @returns string Empty string if there exists no member with that TID. Otherwise, the full name of the member.
 */
if (!function_exists('construct_name')) {
    function construct_name($c, $t)
    {
        $sql_get_name = "SELECT CONCAT((SELECT TEXT FROM ROSTER WHERE TID = $t AND T_ATTRIBUTE = 'FirstName'), 
    ' ',(SELECT TEXT FROM ROSTER WHERE TID = $t AND T_ATTRIBUTE = 'LastName')) AS 'R'";
        $result_get_name = $c->query($sql_get_name);

        if ($result_get_name !== FALSE) {
            return $result_get_name->fetch_assoc()['R'];
        } else return '';
    }
}

/**
 * Update the schedule with the given agenda tuple $a.
 *
 * @param $c mysqli Connection to our database.
 * @param $func string Determines if we perform an update or an insert. Exists in space ['update', 'insert'].
 * @param $m int Meeting ID of the agenda to update.
 * @param $a array Result query that holds our ITEM_NUMBER and AType for the entry to insert.
 * @param $e string Text to update with. This is either a TID or some TEXT.
 * @param $r array Response array to store any error messages in.
 * @return array Our response array with our agenda.
 */
if (!function_exists('update_schedule')) {
    function update_schedule($c, $func, $m, $a, $e, $r)
    {
        $item_num = $a['ITEM_NUMBER'];

        if ($a['AType'] == 'DUTY' && $func == 'update') {
            $full_name = construct_name($c, $e);

            // Verify that the user actually exists.
            if ($full_name != '') {
                $sql_update = "UPDATE SCHEDULE SET TID = $e, TEXT = '$full_name' WHERE MID = $m AND 
                               ITEM_NUMBER = $item_num";
                if ($c->query($sql_update) === FALSE) {
                    die('Error: ' . $c->error);
                }

            } else $r['msg'] .= 'Member with TID = ' . $e . ' not found. ';

        } elseif ($a['AType'] == 'DUTY' && $func == 'insert') {
            $full_name = construct_name($c, $e);

            // Verify that the user actually exists.
            if ($full_name != '') {
                $sql_insert = "INSERT IGNORE INTO SCHEDULE (MID, ITEM_NUMBER, TID, TEXT) VALUES ($e, $item_num, $m, 
                               '$full_name')";

                if ($c->query($sql_insert) === FALSE) {
                    die('Error: ' . $c->error);
                }
            } else $r['msg'] .= 'Member with TID = ' . $e . ' not found. ';

        } elseif ($a['AType'] == 'ATTRIBUTE' && $func == 'update') {
            $stmt = $c->prepare("UPDATE SCHEDULE SET TID = 0, TEXT = ? WHERE MID = $m AND ITEM_NUMBER = $item_num");
            $stmt->bind_param('s', $e);

            if (!$stmt->execute()) {
                die('Error: ' . $c->error);
            }
        } elseif ($a['AType'] == 'ATTRIBUTE' && $func == 'insert') {
            $stmt_insert = $c->prepare("INSERT IGNORE INTO SCHEDULE (TID, TEXT, MID, ITEM_NUMBER) VALUES (0, ?, $m, 
                                            $item_num)");
            $stmt_insert->bind_param('s', $e);

            if (!$stmt_insert->execute()) {
                die('Error: ' . $c->error);
            }

        } else die("Not quite sure how you got here... Congrats??");

        return $r;
    }
}

if (!preg_match($reg_date, $date_in)) {
    $response['msg'] = "Given date not in correct format.";
} elseif (count($new_item_list) !== count($new_entry_list)) {
    $response['msg'] = "Input list lengths do not match.";

} else {
    $stmt = $conn->prepare("SELECT MID FROM MEETING_DATES WHERE DATE(MDate) = ?");
    $stmt->bind_param('s', $date_in);
    $stmt->execute();
    $result = $stmt->get_result();

    // Convert our date into a MID.
    if ($u = $result->fetch_assoc()) {
        $sql_date_to_mid = "SELECT MID FROM MEETING_DATES WHERE DATE(MDate) = $date_in";
        $mid = $u['MID'];
        $response['MID'] = $mid;

        // For each item, find the item number.
        foreach ($new_item_list as $i => $p) {
            $sql_select_item_num = "SELECT ITEM_NUMBER, AType FROM AGENDA WHERE ITEM = '$p'";

            // Update our schedule. Check if we need to perform an update or an insert.
            if ($a = $conn->query($sql_select_item_num)->fetch_assoc()) {
                $item_num = $a['ITEM_NUMBER'];
                $sql_check_exists = "SELECT TID FROM SCHEDULE WHERE MID = $mid AND ITEM_NUMBER = $item_num";

                if ($conn->query($sql_check_exists)->num_rows == 1) {
                    $response = update_schedule($conn, 'update', $mid, $a, $new_entry_list[$i], $response);
                } else $response = update_schedule($conn, 'insert', $mid, $a, $new_entry_list[$i], $response);

            } else {
                $response['msg'] .= "Item " . $p . " does not exist in AGENDA. ";
                break;
            }
        }

        // Grab our agenda and put this in our response array.
        $response = grab_agenda($conn, $mid, $response);

    } else $response['msg'] = 'There is no meeting entry on the date ' . $date_in . ".";
}

// If at this point our message is empty, return 'Success'.
if ($response['msg'] == '') {
    $response['msg'] = 'Success';
}

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;

?>