<?php
/**
 * Grab all agenda information attach the meeting on the given date. There are four comma separated lists here: the
 * item list, item number list, type list, and the text list. The elements for all lists map to the other lists through
 * indices.
 *
 * Requested Variables: MDate -> The date to retrieve agenda for. Input must be in the format YYYY-MM-DD.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      MDate -> Meeting date specified by the user.
 *                      MID -> MID of the meeting date specified by the user.
 *                      ItemList -> Either the name of the duty or the name of the attribute for an text, as a
 *                      comma separated list.
 *                      ItemNumberList -> The item number of the duty or attribute for a given text, as a comma
 *                      separated list.
 *                      TypeList -> The type of item corresponding the given text. In space ['ATTRIBUTE', 'DUTY'].
 *                      TextList -> The text corresponding to the duty/attribute and the item number, as a comma
 *                      separated list.
 */
include('_global.php');

// Grab our input date.
$date_in = $_REQUEST['MDate'];

// Verify that our date is in the correct format. Magic! Woah! Das ist sehr lang!
$reg_date = "/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012]
)\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|
((16|[2468][048]|[3579][26])00))\-02\-29))$/";

// Define the default values for our response array.
$response = array_fill_keys(array('msg', 'MDate', 'MID', 'ItemList', 'ItemNumberList', 'TypeList', 'TextList'), '');
$response['MDate'] = $date_in;

if (!preg_match($reg_date, $date_in)) {
    $response['msg'] = "Given date not in correct format.";

} else {
    // Convert our date into an MID.
    $mid = '';
    $sql_date_to_mid = "SELECT MID FROM MEETING_DATES WHERE MDate = '$date_in'";
    if ($u = $conn->query($sql_date_to_mid)->fetch_assoc()) {
        $mid = $u['MID'];
        $response['MID'] = $mid;

        // Grab all the items an agenda requires. We order this the specified agenda order.
        $sql_select_items = "SELECT ITEM, ITEM_NUMBER, AType FROM AGENDA ORDER BY AOrder ASC";
        $result_select_items = $conn->query($sql_select_items);
        while ($u = $result_select_items->fetch_assoc()) {
            $response['ItemList'] .= addslashes($u['ITEM']) . ",";
            $response['ItemNumberList'] .= $u['ITEM_NUMBER'] . ",";
            $response['AType'] .= $u['AType'] . ",";
            $item_number = $u['ITEM_NUMBER'];

            // Search for this item at the given meeting date. If we have nothing, insert only a comma.
            $sql_select_text = "SELECT TEXT FROM SCHEDULE WHERE MID = $mid AND ITEM_NUMBER = $item_number";
            if ($w = $conn->query($sql_select_text)->fetch_assoc()) {
                $response['TextList'] .= addslashes($w['TEXT']) . ",";
            } else $response['TextList'] .= ",";
        }

        // Specify our error message. Hopefully this is non empty.
        if ($response['ItemList'] != '' && $response['ItemNumberList'] != '' && $response['AType'] != ''
            && $response['TextList'] != '') {
            $response['msg'] = 'Success';
        } else $response['msg'] = 'No agenda items found';

        // Remove trailing commas in our lists.
        foreach (array('ItemList', 'ItemNumberList', 'TypeList', 'TextList') as $v) {
            $response[$v] = rtrim($response[$v], ',');
        }

    } else $response['msg'] = 'There is no meeting entry on the date ' . $date_in . ".";
}

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;
?>