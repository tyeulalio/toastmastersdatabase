<?php
/**
 * Destroy our session.
 *
 * Response (JSON):     msg -> Response JSON. If all goes well, this field is "Success".
 *                      TID -> ID of the user who just logged out.
 */
include("_global.php");
$tid_in = $session_tid * 1;

// Our response message.
$msg = '';

// We only destroy our session if a TID is specified.
if ($tid_in > 0) {
    $msg = "Success";
    session_unset();
    session_destroy();
} else $msg = "No user is logged in.";

header('Content-Type: application/json');
$json = json_encode(["msg" => $msg, "TID" => $tid_in], JSON_PRETTY_PRINT);
echo $json;
?>