<?php

include("_global.php");	// starts session, returns connector to database $conn 

$tid = $session_tid;  //$session_tid

$duty = $_REQUEST['duty'];
$desireOld = $_REQUEST['desireOld'];
$desireNew = $_REQUEST['desireNew'];
$function = $_REQUEST['Function']; // Function can be 'retrieve' or 'update'

$desires=new stdClass();
$desireAr= Array();


if($tid>0)
{

	if($function=='retrieve'){
		$sql="SELECT * FROM DESIRES WHERE TID='$tid'";

		$sqlQuery = $conn->query($sql);
		while($row=$sqlQuery-> fetch_assoc()){
			$datr=$row[DUTY];
			$ddes=$row[DESIRE];
			$desireAr[$datr]=$ddes;
		}
		$message = "Duties showing for TID = '$tid'";
	
	}
	else if($function=='update') {
	

		$sql="SELECT * FROM DESIRES WHERE TID='$tid'";

		$sqlQuery = $conn->query($sql);
		while($row=$sqlQuery-> fetch_assoc()){
			$datr=$row[DUTY];
			$ddes=$row[DESIRE];
			$desireAr[$datr]=$ddes;
		}	
	if($desireAr!='')
	{

		$message = "Duty desires for TID = '$tid' ";


		if($duty != '' && $desireOld==$desireAr[$duty] && $desireOld != $desireNew)
		{
			

			$sql="UPDATE DESIRES SET DESIRE = '$desireNew' WHERE TID='$tid' AND DUTY = '$duty'";
			

		
			if($conn->query($sql) != 0) { 
				$message .= "Desire to be '$duty' for '$tid' updated from '$desireOld' to '$desireNew'. ";
			}
			else {
				$message .= "There was a conflict. No values were updated. ";
			}


			$sql="SELECT * FROM DESIRES WHERE TID='$tid'";

			$sqlQuery = $conn->query($sql);


			while($row=$sqlQuery-> fetch_assoc()){
				$datr=$row[DUTY];
				$ddes=$row[DESIRE];
				$desireAr[$datr]=$ddes;
			}
		}else{
		
		$message = "Enter correct data for duty and desire fields";

		}
		
	
	}
	
	else
	{
		$message = "Person not found.";
	}
	}
	} else $message = "No tid specified.";

$json = json_encode(["session_tid" => $session_tid, "message" => $message, "desires" => $desireAr], JSON_PRETTY_PRINT);

header('Content-Type: application/json');
echo $json;

?>