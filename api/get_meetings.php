<?php
/**
 * Retrieve the MIDs and dates for all meetings between the specified dates.
 *
 * Requested Variables: DateBegin -> Specify the meeting date of availabilities to begin searching with. Input must be
 *                      in the format YYYY-MM-DD.
 *                      DateEnd -> Specify the meeting date of availabilities to end searching with. Input must be
 *                      in the format YYYY-MM-DD.
 *
 * Response (JSON):     msg -> String response. Success gives 'Success' as the string here.
 *                      MIDList -> Comma separated list of MIDs corresponding to the requested meetings.
 *                      MDateList -> Comma separated list of dates. Indices map to MIDList.
 */
include('_global.php');

// Grab our dates.
$begin_date_in = $_REQUEST['DateBegin'];
$end_date_in = $_REQUEST['DateEnd'];

// Verify that our dates are in the correct format. Magic! Woah! Das ist sehr lang!
$reg_date = "/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012]
)\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|
((16|[2468][048]|[3579][26])00))\-02\-29))$/";

// Define the default values for our response array.
$response = array_fill_keys(array('msg', 'MIDList', 'MDateList'), '');

if (!preg_match($reg_date, $begin_date_in) || !preg_match($reg_date, $end_date_in)) {
    $response['msg'] = "Dates not in the correct format.";
} elseif ($begin_date_in == '' && $end_date_in == '') {
    $response['msg'] = "BeginDate and EndDate not specified.";

} else {
    // User must be logged in and specify two dates.
    $sql_mid_list = "SELECT MID, MDate FROM MEETING_DATES WHERE MDate BETWEEN '$begin_date_in' AND '$end_date_in'";
    $result_mid_list = $conn->query($sql_mid_list);

    // Specify our error message.
    if ($result_mid_list->num_rows > 0) {
        $response['msg'] = 'Success';
    } else {
        $response['msg'] = 'No meetings found between ' . $begin_date_in . " and " . $end_date_in;
    }

    // Search for MIDs between the two dates.
    while ($mid_list = $result_mid_list->fetch_assoc()) {
        $mdate = $mid_list['MDate'];
        $mid = $mid_list['MID'];

        // Append to our lists.
        $response['MDateList'] .= $mdate . ",";
        $response['MIDList'] .=$mid . ",";
    }

    // Remove the trailing commas from our lists.
    $response['MDateList'] = rtrim($response['MDateList'], ',');
    $response['MIDList'] = rtrim($response['MIDList'], ',');
}

header('Content-Type: application/json');
$json = json_encode($response, JSON_PRETTY_PRINT);
echo $json;
?>