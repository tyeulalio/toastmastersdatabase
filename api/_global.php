<?php
/**
 * Figure out who's session this is, and connect to our database.
 * ---------------------------------------------------------------------------------------------------------------------
 * To fit this for your needs:
 * 1) Create a PHP file in this folder called '_sqlUser.php'.
 * 2) Insert the tags '<?php       ?>'.
 * 3) Between the tags, insert the lines '$sqlHost = **DATABASE HOSTNAME GOES HERE**'.
 *                                       '$sqlUser = **DATABASE ADMIN USERNAME GOES HERE**'.
 *                                       '$sqlPass = **DATABASE ADMIN PASSWORD GOES HERE**'.
 * 4) Replace 'glennga_DBA_TOAST' with the name of your MySQL database.
 * ---------------------------------------------------------------------------------------------------------------------
 *
 * Added to Global: conn -> Connection to our database.
 *                  session_tid -> ID of the user with the current session.
 *
 */
include('_sqlUser.php');

// Grab the TID of our user.
session_start();
$session_tid = $_SESSION['TID'];

// Define our working database name.
$dbName = 'glennga_DBA_TOAST';

// Connect to our host. Ensure that we can connect.
$conn = new mysqli($sqlHost, $sqlUser, $sqlPass, $dbName);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>